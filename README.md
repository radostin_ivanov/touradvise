# README #

* Tour Advise

* Request example 
    http://localhost:8081/country-tour/api/advise?country=BG&budgetPerCountry=80&totalBudget=750&currency=EUR
    
* Response 
    {
       "countryAdvise":[
          {
             "country":"Bulgaria",
             "country2Code":"BG",
             "amount":156.352,
             "currency":"BGN"
          },
          {
             "country":"Greece",
             "country2Code":"GR",
             "amount":80.0,
             "currency":"EUR"
          },
          {
             "country":"Macedonia (the former Yugoslav Republic of)",
             "country2Code":"MK",
             "amount":4931.7512,
             "currency":"MKD"
          },
          {
             "country":"Romania",
             "country2Code":"RO",
             "amount":386.3596,
             "currency":"RON"
          },
          {
             "country":"Serbia",
             "country2Code":"RS",
             "amount":9398.95096,
             "currency":"RSD"
          },
          {
             "country":"Turkey",
             "country2Code":"TR",
             "amount":579.9143200000001,
             "currency":"TRY"
          }
       ],
       "leftover":270.0,
       "lackAmount":0.0,
       "currency":"EUR",
       "allTourCount":2
    }
* Security - Google Oauth2 Login 2
