package com.countries.tour.dto;

import java.util.List;

public class Advise {

    private List<CountryTourAdvisor> countryAdvise;

    private Double leftover;

    private Double lackAmount;

    private String currency;

    private Long allTourCount;

    public Advise(){

    }


    public Double getLeftover() {
        return leftover;
    }

    public void setLeftover(Double leftover) {
        this.leftover = leftover;
    }

    public List<CountryTourAdvisor> getCountryAdvise() {
        return countryAdvise;
    }

    public void setCountryAdvise(List<CountryTourAdvisor> countryAdvise) {
        this.countryAdvise = countryAdvise;
    }

    public Long getAllTourCount() {
        return allTourCount;
    }

    public void setAllTourCount(Long allTourCount) {
        this.allTourCount = allTourCount;
    }

    public Double getLackAmount() {
        return lackAmount;
    }

    public void setLackAmount(Double lackAmount) {
        this.lackAmount = lackAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
