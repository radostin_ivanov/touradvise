package com.countries.tour.dto;

public class CountryTourAdvisor {

    private String country;
    private String country2Code;
    private Double amount;
    private String currency;

    public CountryTourAdvisor(){

    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCountry2Code() {
        return country2Code;
    }

    public void setCountry2Code(String country2Code) {
        this.country2Code = country2Code;
    }

}
