package com.countries.tour.controller;

import com.countries.tour.dto.Advise;
import com.countries.tour.exception.CurrencyExchangeException;
import com.countries.tour.exception.GeoLocationIntegrationException;
import com.countries.tour.service.CountryTourAdvisorService;
import com.neovisionaries.i18n.CountryCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path = "api")
public class CountryTourController {

    @Autowired
    CountryTourAdvisorService countryTourAdvisorService;

    @GetMapping("/advise")
    public ResponseEntity<Advise> tourAdvise(@RequestParam(name = "country", required = true) String country,
                                             @RequestParam(name = "budgetPerCountry", required = true) Double budgetPerCountry,
                                             @RequestParam(name = "totalBudget", required = true) Double totalBudget,
                                             @RequestParam(name = "currency", required = true) String currency) throws GeoLocationIntegrationException, CurrencyExchangeException {


        return new ResponseEntity<>(countryTourAdvisorService.tourAdvise(
                CountryCode.getByCode(country),
                budgetPerCountry,
                totalBudget,
                currency), HttpStatus.OK);

    }

}
