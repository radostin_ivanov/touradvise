package com.countries.tour.service;

import com.countries.tour.dto.geolocation.CountryInfo;
import com.countries.tour.exception.GeoLocationIntegrationException;
import com.neovisionaries.i18n.CountryCode;

import java.util.List;

public interface GeoLocationService {


    List<CountryInfo> getTargetCountries(CountryCode isoCountryCode) throws GeoLocationIntegrationException;

}
