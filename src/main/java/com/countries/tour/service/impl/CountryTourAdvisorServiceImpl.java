package com.countries.tour.service.impl;

import com.countries.tour.dto.Advise;
import com.countries.tour.dto.CountryTourAdvisor;
import com.countries.tour.dto.currency.Rate;
import com.countries.tour.dto.geolocation.CountryInfo;
import com.countries.tour.exception.CurrencyExchangeException;
import com.countries.tour.exception.GeoLocationIntegrationException;
import com.countries.tour.service.CountryTourAdvisorService;
import com.countries.tour.service.CurrencyExchangeRateService;
import com.countries.tour.service.GeoLocationService;
import com.neovisionaries.i18n.CountryCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CountryTourAdvisorServiceImpl implements CountryTourAdvisorService {

    @Autowired
    GeoLocationService geoLocationService;

    @Autowired
    CurrencyExchangeRateService currencyExchangeRateService;


    @Override
    public Advise tourAdvise(CountryCode isoCountryCode, Double budgetPerCountry, Double totalBudget, String currency) throws GeoLocationIntegrationException, CurrencyExchangeException {

        List<CountryInfo> targetCountries = geoLocationService.getTargetCountries(isoCountryCode);

        Advise advise = new Advise();

        List<CountryTourAdvisor> countryTourAdvise = new ArrayList<>();

        Double leftOverBudget = totalBudget;

        for (CountryInfo countryInfo : targetCountries) {
            CountryTourAdvisor countryTourAdvisor = new CountryTourAdvisor();
            countryTourAdvisor.setCountry(countryInfo.getName());
            countryTourAdvisor.setCountry2Code(countryInfo.getAlpha2Code());

            Rate rate = currencyExchangeRateService.exchangeRate(currency, countryInfo.getCurrencies().get(0).getCode());

            countryTourAdvisor.setCurrency(rate.getCurrency());
            countryTourAdvisor.setAmount(rate.getRate() * budgetPerCountry);

            countryTourAdvise.add(countryTourAdvisor);

            leftOverBudget = leftOverBudget - budgetPerCountry;
        }

        advise.setCountryAdvise(countryTourAdvise);
        advise.setLeftover(leftOverBudget > 0 ? leftOverBudget : 0);
        advise.setLackAmount(leftOverBudget > 0 ? 0 : Math.abs(leftOverBudget));
        advise.setCurrency(currency);
        advise.setAllTourCount(leftOverBudget < 0 ? 0 : Math.round(totalBudget/(targetCountries.size()*budgetPerCountry)));

        return advise;

    }

}
