package com.countries.tour.service.impl;

import com.countries.tour.dto.geolocation.CountryInfo;
import com.countries.tour.exception.GeoLocationIntegrationException;
import com.countries.tour.service.GeoLocationService;
import com.neovisionaries.i18n.CountryCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class GeoLocationServiceImpl implements GeoLocationService {


    @Value("${countries.info.url}")
    private String url;

    @Autowired
    RestTemplate client;


    @Override
    public List<CountryInfo> getTargetCountries(CountryCode isoCountryCode) throws GeoLocationIntegrationException {
        List<CountryInfo> targetCountries = new ArrayList<CountryInfo>();

        CountryInfo countryInfo = getCountryInfo(isoCountryCode);

        targetCountries.add(countryInfo);

        for(String borderCountry : countryInfo.getBorders()){
            targetCountries.add(getCountryInfo(CountryCode.getByCode(borderCountry)));
        }

        return targetCountries;
    }

    private CountryInfo getCountryInfo(CountryCode isoCountryCode) throws GeoLocationIntegrationException{
        ResponseEntity<CountryInfo> resp = client.exchange(url + "/" + isoCountryCode.getAlpha3(), HttpMethod.GET, null, CountryInfo.class);

        if (HttpStatus.OK == resp.getStatusCode()) {
            return resp.getBody();
        } else {
            throw new GeoLocationIntegrationException();
        }
    }

}
