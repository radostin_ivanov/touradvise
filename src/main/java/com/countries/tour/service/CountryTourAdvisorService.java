package com.countries.tour.service;

import com.countries.tour.dto.Advise;
import com.countries.tour.exception.CurrencyExchangeException;
import com.countries.tour.exception.GeoLocationIntegrationException;
import com.neovisionaries.i18n.CountryCode;

public interface CountryTourAdvisorService {

    Advise tourAdvise(CountryCode isoCountryCode, Double budgetPerCountry, Double totalBudget, String currency) throws GeoLocationIntegrationException, CurrencyExchangeException;
}
