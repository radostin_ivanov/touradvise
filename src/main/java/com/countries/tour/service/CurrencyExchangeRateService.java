package com.countries.tour.service;

import com.countries.tour.dto.currency.Rate;
import com.countries.tour.exception.CurrencyExchangeException;

public interface CurrencyExchangeRateService {

    Rate exchangeRate(String baseCurrency, String targetCurrencies) throws CurrencyExchangeException;
}
